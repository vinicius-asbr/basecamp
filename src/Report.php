<?php

namespace BCReports;

class Report
{

    private $pdo;

    private $team;

    public function __construct($pdo, $team)
    {
        $this->pdo = $pdo;
        $this->team = $team;
        $this->pdo->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING );
    }

    public function getPingPongs($team)
    {
        //Subtraio 2 pq tem o fluxo normal de passar pro dev e devolver para o BM
        $stmt = $this->pdo->prepare("SELECT CONCAT(bucket_name, ' - ', target) as todo, COUNT(1) - 2 as ping_pong FROM basecamp
            WHERE (action REGEXP ':team' OR creator_dept = '$team') AND action REGEXP 'assigned|complete|added|re-opened'
            AND NOT (creator_dept = '$team' AND action REGEXP 'triagem|sprint')
            GROUP by CONCAT(bucket_name, ' - ', target) ORDER BY ping_pong desc limit 5");

        $stmt->execute([
            "team" => implode(array_keys($this->getTeam($team)), "|")
        ]);

        $result = $stmt->fetchAll();
        foreach ($result as $row) {
            $arr[$row["todo"]] = (int)$row['ping_pong'];
        }
        return $arr;
    }

    public function getCompletedTodos($team)
    {
        $stmt = $this->pdo->prepare("SELECT count(*) as total FROM basecamp
            WHERE CONCAT(bucket_name, ' - ', target) IN (
            	SELECT CONCAT(bucket_name, ' - ', target)FROM basecamp
            	WHERE (action REGEXP ':team' OR creator_dept = '$team') AND action REGEXP 'assigned|complete|added|re-opened'
            	AND NOT (creator_dept = '$team' AND action REGEXP 'triagem|sprint')
            ) AND id IN (SELECT MAX(id) FROM basecamp GROUP BY CONCAT(bucket_name, ' - ', target))
            AND action NOT REGEXP '".implode(array_keys($this->getTeam($team)), "|")."|sprint|triagem|bloqueado|deleted|commented'; ");

        $stmt->execute([
            "team" => implode(array_keys($this->getTeam($team)), "|")
        ]);

        return $stmt->fetch()["total"];
    }

    public function getTodosByProject($team)
    {
        $stmt = $this->pdo->prepare("SELECT bucket_name as name, COUNT(DISTINCT target) as total FROM basecamp
            WHERE (action REGEXP ':team' OR creator_dept = '$team') AND action REGEXP 'assigned|complete|added|re-opened'
            AND NOT (creator_dept = '$team' AND action REGEXP 'triagem|sprint')
            GROUP by bucket_name ORDER BY total desc limit 5");

        $stmt->execute([
            "team" => implode($this->getTeam($team), "|")
        ]);

        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $arr[$row["name"]] = (int)$row['total'];
        }
        return $arr;
    }

    public function getTodosToDeveloper($team)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM basecamp
        WHERE action REGEXP 'added a to-do and assigned' AND action REGEXP ':team'  ");

        $stmt->execute([
            "team" => implode(array_keys($this->getTeam($team)), "|")
        ]);

        return (int)$stmt->fetch()["total"];
    }

    public function getTeam($subteam = null)
    {
        return  (is_null($subteam)) ? $this->team : $this->team[$subteam];
    }

    public function getDepts()
    {
        return  array_keys($this->team);
    }

    public function getLeftoverTodos($team)
    {
        $stmt = $this->pdo->prepare("SELECT id FROM basecamp
                WHERE CONCAT(bucket_name, ' - ', target) IN (
                	SELECT CONCAT(bucket_name, ' - ', target) FROM basecamp
                	WHERE (action REGEXP '".implode(array_keys($this->getTeam($team)), "|")."' OR creator_dept = '$team') AND action REGEXP 'assigned|complete|added|re-opened'
                	AND NOT (creator_dept = '$team' AND action REGEXP 'triagem|sprint')
                ) AND id IN (SELECT MAX(id) FROM basecamp GROUP BY CONCAT(bucket_name, ' - ', target))
                AND action REGEXP '".implode(array_keys($this->getTeam($team)), "|")."|sprint|triagem|bloqueado|deleted|commented'") ;

        $stmt->execute();

        $result = $stmt->fetchAll();
        $sum = 0;
        foreach ($result as $row) {
            $sum += (int)$row['id'];
        }
        return $sum;
    }

    public function findDept($user)
    {
        foreach ($this->getTeam() as $dept => $team) {
            foreach ($team as $_user) {
                if($_user == $user) return $dept;
            }
        }
        return "BM";
    }

}
