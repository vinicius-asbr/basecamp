<?php

require 'vendor/Autoload.php';

set_time_limit(1000);

 $dotenv = new Dotenv\Dotenv(__DIR__);
 $dotenv->load();

$dbconfig = require __DIR__."/configs/database.php";
$pdo = new \PDO("mysql:host=".$dbconfig['host'].";dbname=".$dbconfig['database'], $dbconfig["user"], $dbconfig["password"]);

$report = new \BCReports\Report($pdo);

$client = \Basecamp\BasecampClient::factory(require __DIR__."/configs/basecamp.php");

$apiReturn = [];
$page = 1;
$events = [];
$sinceDate = (new DateTime())
    ->add(DateInterval::createFromDateString('yesterday'))
    ->format('Y-m-d');

do {
    echo "Buscando página $page\r\n";
    $apiReturn = $client->getGlobalEvents([
        'since' => $sinceDate.' 00:00:00-02:00',
        'page' => $page
    ]);
    $events = array_merge($events, $apiReturn);
    sleep(1);
    $page++;
} while (!empty($apiReturn));


foreach ($events as $event) {
    $date_created = \DateTime::createFromFormat('Y-m-d\TH:i:s.uT', $event['created_at']);
    $date_updated = \DateTime::createFromFormat('Y-m-d\TH:i:s.uT', $event['updated_at']);
    $target_user = strpos($event['action'], ' to ') ? str_replace('<span>:</span>', '', substr($event['action'], strpos($event['action'], ' to ') + 4)) : '' ;
    $dept = $report->getDept($event['creator']['name']);

    $stmt = $pdo->prepare("INSERT INTO basecamp (id_basecamp, created_at, updated_at, action,target, target_user,html_url, creator_name,creator_dept,bucket_name)
        	VALUES (:id_basecamp, :created_at, :updated_at, :action, :target, :target_user, :html_url, :creator_name, :creator_dept, :bucket_name)");

    $stmt->execute([
            "id_basecamp" => $event['id'],
            "created_at" => $date_created->format('Y-m-d H:i:s'),
            "updated_at" => $date_updated->format('Y-m-d H:i:s'),
            "action" => $event['action'],
            "target" => $event['target'],
            "target_user" => $target_user,
            "html_url" => $event['html_url'],
            "creator_name" =>  $event['creator']['name'],
            "creator_dept" => $dept,
            "bucket_name" => $event['bucket']['name']
    ]);

}
