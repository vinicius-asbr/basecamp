<?php

return [
    "designers" => [
        'Bruno Furtado',
        'Roney Lacerda',
        'Marina Martins',
        'Maynnara Jorge'
    ],
    "IT" => [
        'Guilherme Assemany',
        'Vinicius Carvalho',
        'Arthur Gandriann' ,
        'André Silva',
        'Raíssa Lima',
        'Faiçal Assad',
        'Paulo Frois'
    ]
];
