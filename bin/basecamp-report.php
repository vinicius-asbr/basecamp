<?php

require '../vendor/Autoload.php';
require '../bootstrap.php';

foreach ($report->getDepts() as  $subteam) {
    $todosByProject = $report->getTodosByProject($subteam);
    $todosPingPong = $report->getPingPongs($subteam);

    var_dump([
        "Tarefas que passaram pelo TI " => array_sum($todosByProject),
        "Top 5 Projetos com maior numero de tarefas" => $todosByProject,
        "Ping Pong" => array_sum($todosPingPong),
        "Tarefas com Ping Pong" => $todosPingPong,
        "Tarefas Completas" => $report->getCompletedTodos($subteam),
        "Tarefas Restante" => $report->getLeftoverTodos($subteam),
        "Tarefas mandadas para o Programador" => $report->getTodosToDeveloper($subteam)
    ]);

}
