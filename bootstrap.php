<?php

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$dbconfig = require __DIR__."/configs/database.php";
$bcconfig = require __DIR__."/configs/basecamp.php";
$teamconfig = require __DIR__."/configs/team.php";

$pdo = new \PDO("mysql:host=".$dbconfig['host'].";dbname=".$dbconfig['database'], $dbconfig["user"], $dbconfig["password"],[
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
]);

$report = new BCReports\Report($pdo,$teamconfig);
