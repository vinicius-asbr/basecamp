<?php

return [
    'auth' => 'http',
    'username' => getenv("BC_USER"),
    'password' => getenv("BC_PASSWORD"),
    'user_id' => '2187345',
    'app_name' => 'Report Test',
    'app_contact' => '<email>'
];
