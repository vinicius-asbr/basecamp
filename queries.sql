-- Total de tarefas que passaram pelo TI - 92 tarefas

SELECT bucket_name, COUNT(DISTINCT target) FROM basecamp_201612
WHERE (action REGEXP 'raíssa|guilherme|arthur' OR creator_dept = 'TI') AND action REGEXP 'assigned|complete|added|re-opened'
AND NOT (creator_dept = 'TI' AND action REGEXP 'triagem|sprint')
GROUP by bucket_name;

-- Total ping pong

SELECT CONCAT(bucket_name, ' - ', target) as tarefa, COUNT(1) - 2 as ping_pong FROM basecamp_201612 -- Subtraio 2 pq tem o fluxo normal de passar pro dev e devolver para o BM
WHERE (action REGEXP 'raíssa|guilherme|arthur' OR creator_dept = 'TI') AND action REGEXP 'assigned|complete|added|re-opened'
AND NOT (creator_dept = 'TI' AND action REGEXP 'triagem|sprint')
GROUP by CONCAT(bucket_name, ' - ', target);

-- Total de tarefas "completas" - 73

SELECT * FROM basecamp_201612
WHERE CONCAT(bucket_name, ' - ', target) IN (
	SELECT CONCAT(bucket_name, ' - ', target)FROM basecamp_201612
	WHERE (action REGEXP 'raíssa|guilherme|arthur' OR creator_dept = 'TI') AND action REGEXP 'assigned|complete|added|re-opened'
	AND NOT (creator_dept = 'TI' AND action REGEXP 'triagem|sprint')
) AND id IN (SELECT MAX(id) FROM basecamp_201612 GROUP BY CONCAT(bucket_name, ' - ', target))
AND action NOT REGEXP 'raíssa|guilherme|arthur|vinicius|sprint|triagem|bloqueado|deleted|commented'; 

-- Resto das tarefas

SELECT * FROM basecamp_201612
WHERE CONCAT(bucket_name, ' - ', target) IN (
	SELECT CONCAT(bucket_name, ' - ', target)FROM basecamp_201612
	WHERE (action REGEXP 'raissa|guilherme|arthur' OR creator_dept = 'TI') AND action REGEXP 'assigned|complete|added|re-opened'
	AND NOT (creator_dept = 'TI' AND action REGEXP 'triagem|sprint')
) AND id IN (SELECT MAX(id) FROM basecamp_201612 GROUP BY CONCAT(bucket_name, ' - ', target))
AND action REGEXP 'raíssa|guilherme|arthur|vinicius|sprint|triagem|bloqueado|deleted|commented';

-- Direto pra Programador

SELECT * FROM basecamp_201612
WHERE action REGEXP 'added a to-do and assigned' AND action REGEXP 'raissa|guilherme|arthur' 

 
